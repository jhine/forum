<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLikeRequest;
use App\Reply;
use Illuminate\Http\Request;

/**
 * Class ReplyLikeController
 *
 * @package App\Http\Controllers
 */
class ReplyLikeController extends Controller
{
    /**
     * ReplyLikeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @param \App\Reply               $reply
     *
     * @return void
     */
    public function store(Request $request, Reply $reply)
    {
        $hasLiked = $reply->likes->contains(function ($like) {
            return $like->user_id === auth()->id();
        });

        if ($hasLiked) {
            $reply->likes()
                ->where('user_id', auth()->id())
                ->delete();
        }

        if (!$hasLiked) {
            $reply->likes()->updateOrCreate([
                'user_id' => auth()->id()
            ]);
        }

        $reply = $reply->refresh();

        if ($request->expectsJson()) {
            return response()->json([
                'total' => $reply->likes->count()
            ]);
        }
    }
}
