<?php

namespace App\Http\View\Composers;

use App\Category;
use Illuminate\View\View;

class CategoryComposer
{
    private $categories;

    /**
     * Create a new profile composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->categories = Category::withCount('threads')
            ->orderBy('name')
            ->get();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->categories);
    }
}
