<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Like
 *
 * @package App
 */
class Like extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function likeable()
    {
        return $this->morphTo();
    }
}
