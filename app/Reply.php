<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Reply
 *
 * @package App
 */
class Reply extends Model
{
    /**
     * @var array
     */
    protected $with = [
        'user', 'likes'
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany('App\Like', 'likeable');
    }

    /**
     * @return mixed
     */
    public function author()
    {
        return $this->user->username;
    }
}
