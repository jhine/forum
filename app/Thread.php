<?php

namespace App;

use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Thread
 *
 * @package App
 */
class Thread extends Model
{
    use HasSlug;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $with = [
        'user'
    ];

    /**
     * Boot
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($thread) {
            $thread->update([
                'slug' => $thread->title
            ]);
        });
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * @param int $limit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastReply()
    {
        return $this->hasOne(Reply::class)
                ->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return string
     */
    public function path()
    {
        return '/threads/' . $this->slug;
    }

    /**
     * @return mixed
     */
    public function author()
    {
        return $this->user->username;
    }

    /** @test
     * @param \App\Reply $reply
     *
     * @return mixed
     */
    public function addReply(Reply $reply)
    {
        return $this->replies()->save($reply);
    }
}
