<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Reply extends Component
{
    /**
     * @var \App\Reply
     */
    public \App\Reply $reply;

    /**
     * Create a new component instance.
     *
     * @param \App\Reply $reply
     */
    public function __construct(\App\Reply $reply)
    {
        $this->reply = $reply;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $totalLikes = $this->reply->likes->count();

        $liked = $this->reply->likes
            ->where('user_id', auth()->id())
            ->isNotEmpty();

        return view('components.reply', compact('totalLikes', 'liked'));
    }
}
