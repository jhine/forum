<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Thread extends Component
{
    /**
     * @var \App\View\Components\Thread
     */
    public \App\Thread $thread;

    /**
     * Create a new component instance.
     *
     * @param \App\View\Components\Thread $thread
     */
    public function __construct(\App\Thread $thread)
    {
        $this->thread = $thread;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.thread');
    }
}
