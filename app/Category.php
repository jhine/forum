<?php

namespace App;

use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Category
 *
 * @package App
 */
class Category extends Model
{
    use HasSlug;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * Boot
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($category) {
            $category->update([
                'slug' => $category->name
            ]);
        });
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    /**
     * @return string
     */
    public function path()
    {
        return '/categories/' . $this->slug;
    }

    /**
     * @return string
     */
    public function hex()
    {
        return $this->hex ? '#' . $this->hex : '#000';
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getDescriptionAttribute($value)
    {
        return Str::limit($value, 200);
    }
}
