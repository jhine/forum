<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Thread;
use App\User;
use Faker\Generator as Faker;

$factory->define(Thread::class, function (Faker $faker) {
    return [
        'category_id' => factory(Category::class),
        'user_id' => factory(User::class),
        'title' => $faker->unique()->word,
        'body' => $faker->paragraphs(3, true)
    ];
});
