<?php

use Illuminate\Database\Seeder;
use App\Thread;
use App\Reply;
use App\User;

class ThreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Thread::class, 5)->create()->each(function (Thread $thread) {
            $replies = factory(Reply::class, 10)->make()->each(function (Reply $reply) {
                $reply->user()->associate(factory(User::class)->create());
            });

            $thread->replies()->saveMany($replies);
        });
    }
}
