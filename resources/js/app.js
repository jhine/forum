require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Like from './components/Like';
import Navigation from './components/Navigation';
import Wysiwyg from './components/Wysiwyg';

const app = new Vue({
    el: '#app',

    components: {
        Like,
        Navigation,
        Wysiwyg,
    }
});
