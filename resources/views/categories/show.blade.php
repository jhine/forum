<x-app>
    <div class="container mx-auto mt-10">
        <h1 class="text-2xl font-bold mb-5 px-2 md:px-0">{{ $category->name }}</h1>
        <div class="flex flex-wrap -mx-2">
            <aside class="w-full mb-5 md:w-1/4 px-2 md:order-2 md:mb-0">
                @include('threads.layouts.aside')
            </aside>
            <main class="w-full md:w-3/4 md:px-2">
                <div class="bg-white rounded p-5 mx-2 md:mx-0">
                    @foreach ($threads as $thread)
                        <x-thread :thread="$thread"></x-thread>
                    @endforeach

                    <div>
                        {{ $threads->links('pagination::tailwind') }}
                    </div>
                </div>
            </main>
        </div>
    </div>
</x-app>
