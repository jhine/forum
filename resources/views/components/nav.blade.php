<navigation inline-template>
    <div class="bg-white">
        <div class="container mx-auto flex flex-wrap md:flex-no-wrap">
            <div class="w-1/2 p-5 md:px-0">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('images/logo-1.svg') }}" alt="Site Logo" class="h-10">
                </a>
            </div>
            <div class="w-1/2 flex items-center justify-end p-5 md:px-0 md:hidden">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="h-5" @click="isVisible = !isVisible"
                     v-if="!isVisible">
                    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="h-5" @click="isVisible = !isVisible"
                     v-else>
                    <path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/>
                </svg>
            </div>
            <div class="flex-col items-center py-3 w-full bg-gray-400 md:py-0 md:flex-row md:justify-end md:bg-transparent"
                 :class="[ isVisible ? 'flex' : 'hidden md:flex' ]"
            >
                <div class="flex flex-col w-full md:flex-row md:justify-end md:w-1/2 ">
                    <a href="{{ route('categories.index') }}" class="p-1 text-center md:mx-3">Categories</a>
                    <a href="{{ route('threads.index') }}" class="p-1 text-center md:mx-3">Threads</a>
                </div>
                <div class="flex flex-col w-full md:flex-row md:flex-1 md:justify-end">
                    @guest
                        <a href="{{ route('login') }}"
                           class="py-1 text-center md:border-r-2 md:border-gray-400 md:pr-3">Login</a>
                        <a href="{{ route('register') }}" class="py-1 text-center md:pl-3">Register</a>
                    @endguest
                    @auth
                        <p class="py-1 text-center md:border-r-2 md:border-gray-400 md:pr-3">Hi, <span
                                    class="font-bold">{{ auth()->user()->username }}</span></p>
                        <a href="#" class="py-1 text-center md:pl-3"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    @endauth
                </div>
            </div>
        </div>

        <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</navigation>
