<div class="bg-white rounded mb-2 p-5 mx-2 md:mx-0">
    <div class="flex justify-between mb-5">
        <p class="font-bold text-sm text-gray-600">{{ $reply->author() }}</p>
        <p class="text-sm text-gray-600">{{ $reply->created_at->diffForHumans() }}</p>
    </div>

    <div>{!! $reply->body !!}</div>

    <div class="flex mt-5">
        <like :reply="{{ $reply }}"></like>
    </div>
</div>
