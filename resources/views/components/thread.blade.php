<div class="flex flex-col mb-5 p-3 border-b justify-between rounded hover:bg-gray-100 md:flex-row">
    <div class="w-3/4 flex flex-col mb-4">
        <h4 class="text-lg">
            <a href="{{ $thread->path() }}">{{ $thread->title }}</a>
        </h4>

        @if ($thread->replies_count)
            <small class="text-gray-600">Last Reply <span class="font-bold">{{ $thread->lastReply->created_at->diffForHumans() }}</span> by <span
                        class="font-bold">{{ $thread->lastReply->author() }}</span>
            </small>
        @else
            <small class="text-gray-600">Created By <span
                        class="font-bold">{{ $thread->author() }}</span> {{ $thread->created_at->diffForHumans() }}
            </small>
        @endif
    </div>
    <div class="flex-1">
        <div class="flex md:justify-end">
            <a href="{{ route('threads.show', ['thread' => $thread->slug]) }}"
               class="flex items-center border-2 rounded px-2 py-1 text-xs mr-2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="h-3 mr-2 fill-current text-gray-600">
                    <path d="M17 11v3l-3-3H8a2 2 0 0 1-2-2V2c0-1.1.9-2 2-2h10a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2h-1zm-3 2v2a2 2 0 0 1-2 2H6l-3 3v-3H2a2 2 0 0 1-2-2V8c0-1.1.9-2 2-2h2v3a4 4 0 0 0 4 4h6z"/>
                </svg>{{ $thread->replies_count }}
            </a>

            <a href="{{ $thread->category->path() }}"
               class="inline-block border-2 border-gray-400 rounded px-2 py-1 text-xs"
            >{{ $thread->category->name }}</a>
        </div>
    </div>
</div>
