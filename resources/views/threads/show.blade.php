<x-app>
    <div class="container mx-auto mt-10 px-2 md:px-0">
        <h1 class="text-2xl font-bold mb-5 px-2 md:px-0">{{ $thread->title }}</h1>
        <div class="flex flex-wrap -mx-2">
            <aside class="w-full mb-5 md:w-1/4 px-2 md:order-2 md:mb-0">
                @include('threads.layouts.aside')
            </aside>
            <main class="w-full md:w-3/4 md:px-2">
                <div class="mb-5">
                    <div class="bg-green-300 rounded p-5 mx-2 md:mx-0">
                        <div class="flex justify-between mb-5">
                            <p class="font-bold text-sm text-green-100">{{ $thread->author() }}</p>
                            <p class="text-sm text-green-100">{{ $thread->created_at->diffForHumans() }}</p>
                        </div>

                        <div class="text-green-900">{!! $thread->body !!}</div>
                    </div>
                </div>

                {{--                Replies--}}
                <h2 class="text-2xl text-gray-600 font-bold mb-5 px-2 md:px-0">Comments</h2>

                @auth
                    <div class="bg-white p-5 mb-2">
                        <form method="post" action="{{ route('threads.replies.store', ['thread' => $thread->slug]) }}">
                            @csrf

                            <wysiwyg class="mb-2 trix"></wysiwyg>
                            @error('body')
                                <span class="text-sm text-red-500">{{ $errors->first('body') }}</span>
                            @enderror

                            <div class="flex justify-end">
                                <button type="submit" class="button is-primary">Reply</button>
                            </div>
                        </form>
                    </div>
                @endauth

                @foreach ($thread->replies as $reply)
                    <x-reply :reply="$reply" class="mb-2"></x-reply>
                @endforeach

                <div class="px-3">
                    {{ $thread->replies->links('pagination::tailwind') }}
                </div>
            </main>
        </div>
    </div>
</x-app>
