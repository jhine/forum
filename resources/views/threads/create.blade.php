<x-app>
    <div class="container mx-auto mt-10 px-2 md:px-0">
        <h1 class="text-2xl font-bold mb-5 px-2 md:px-0">Create Thread</h1>
        <div class="flex flex-wrap -mx-2">
            <aside class="w-full mb-5 md:w-1/4 px-2 md:order-2 md:mb-0">
                @include('threads.layouts.aside')
            </aside>
            <main class="w-full md:w-3/4 md:px-2">
                <div class="bg-white rounded p-5 mx-2 md:mx-0">

                    <form action="{{ route('threads.store') }}" method="post">
                        @csrf

                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label for="email" class="block font-bold mb-1 md:mb-0 pr-4 text-gray-600 is-required">
                                    Title
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input type="text" name="title" id="title"
                                       class="appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white"
                                       value="{{ old('title') }}"
                                >
                                @error('title')
                                <span class="text-sm text-red-500">{{ $errors->first('title') }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label for="category_id" class="block font-bold mb-1 md:mb-0 pr-4 text-gray-600 is-required">
                                    Category
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <select name="category_id" id="category_id"
                                        class="appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white"
                                >
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="text-sm text-red-500">{{ $errors->first('category_id') }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label for="email" class="block font-bold mb-1 md:mb-0 pr-4 text-gray-600 is-required">
                                    Body
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <wysiwyg></wysiwyg>
                                @error('body')
                                <span class="text-sm text-red-500">{{ $errors->first('body') }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="md:flex md:items-center">
                            <div class="md:w-1/3"></div>
                            <div class="md:w-2/3">
                                <button class="button is-primary" type="submit">
                                    Create Thread
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </main>
        </div>
    </div>
</x-app>
