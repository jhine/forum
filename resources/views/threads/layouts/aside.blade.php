<div class="hidden md:block bg-white p-10 rounded">
    <h4 class="text-xl font-bold mb-3">Categories</h4>
    @foreach ($categories as $category)
        <a href="{{ $category->path() }}" class="flex items-center mb-2 text-sm">
            <span class="inline-block h-3 w-3 rounded-full mr-3 bg-gray-600 "></span>{{ $category->name }}
        </a>
    @endforeach
</div>
