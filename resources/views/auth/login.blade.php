<x-app>
    <div class="container mx-auto mt-10">
        <h1 class="text-2xl font-bold mb-5 px-2 md:px-0">Login</h1>
        <div class="bg-white rounded p-10 mx-2 md:mx-0">

            <form action="{{ route('login') }}" method="post" class="w-full">
                @csrf

                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">
                        <label for="email" class="block font-bold mb-1 md:mb-0 pr-4 text-gray-600 is-required">
                            Email Address
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <input type="text" name="email" id="email"
                               class="appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white"
                               value="{{ old('email') }}"
                        >
                        @error('email')
                        <span class="text-sm text-red-500">{{ $errors->first('email') }}</span>
                        @enderror
                    </div>
                </div>

                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">
                        <label for="password" class="block font-bold mb-1 md:mb-0 pr-4 text-gray-600 is-required">
                            Password
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <input type="password" name="password" id="password"
                               class="appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white"
                        >
                        @error('password')
                        <span class="text-sm text-red-500">{{ $errors->first('password') }}</span>
                        @enderror
                    </div>
                </div>

                <div class="md:flex md:items-center">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3">
                        <button class="button is-primary" type="submit">
                            Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app>
