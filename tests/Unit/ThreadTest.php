<?php

namespace Tests\Unit;

use App\Category;
use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_thread_belongs_to_a_category()
    {
        $thread = factory(Thread::class)->make();

        $category = factory(Category::class)->create();

        $thread->category()->associate($category);
        $thread->save();

        $this->assertEquals($thread->category->id, $category->id);
    }

    /** @test */
    public function a_thread_can_have_many_replies()
    {
        $thread = factory(Thread::class)->create();

        $replies = factory(Reply::class, 10)->make()->each(function (Reply $reply) {
            $reply->user()->associate(factory(User::class)->create());
        });

        $thread->replies()->saveMany($replies);

        $this->assertCount(10, $thread->replies);
    }

    /** @test */
    public function a_thread_can_add_a_reply()
    {
        $thread = factory(Thread::class)->create();

        $reply = factory(Reply::class)->make([
            'user_id' => factory(User::class),
            'body' => 'foobar'
        ]);

        $thread->addReply($reply);

        $this->assertCount(1, $thread->replies);
        $this->assertDatabaseHas('replies', [
            'thread_id' => $thread->id,
            'body' => 'foobar'
        ]);
    }

    /** @test */
    public function a_thread_has_a_title()
    {
        $thread = factory(Thread::class)->create([
            'title' => 'foo'
        ]);

        $this->assertSame('foo', $thread->title);
        $this->assertDatabaseHas('threads', [
            'title' => 'foo'
        ]);
    }

    /** @test */
    public function a_thread_belongs_to_a_user()
    {
        $user = factory(User::class)->create();

        $thread = factory(Thread::class)->make();

        $thread->user()->associate($user);

        $this->assertEquals($user->id, $thread->user->id);
    }

    /** @test */
    public function a_thread_has_an_author()
    {
        $thread = factory(Thread::class)->create();

        $this->assertEquals($thread->user->username, $thread->author());
    }

    /** @test */
    public function a_thread_has_a_path()
    {
        $category = factory(Category::class)->create();

        $thread = factory(Thread::class)->make([
            'title' => 'bar',
        ]);

        $category->threads()->save($thread);

        $this->assertEquals('/threads/bar', $thread->path());
    }

    /** @test */
    public function a_thread_has_a_body()
    {
        $thread = factory(Thread::class)->create([
            'body' => 'foobar'
        ]);

        $this->assertSame('foobar', $thread->body);
    }
}
