<?php

namespace Tests\Unit;

use App\Category;
use App\Like;
use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReplyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private Thread $thread;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->thread = factory(Thread::class)->create();
        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function a_reply_belongs_to_a_thread()
    {
        $reply = factory(Reply::class)->make();

        $reply->thread()->associate($this->thread);
        $reply->user()->associate($this->user);
        $reply->save();

        $this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    public function a_reply_belongs_to_a_user()
    {
        $reply = factory(Reply::class)->make();

        $reply->thread()->associate($this->thread);
        $reply->user()->associate($this->user);
        $reply->save();

        $this->assertEquals($reply->user->id, $this->user->id);
    }

    /** @test */
    public function a_reply_has_an_author()
    {
        $reply = factory(Reply::class)->make();

        $reply->thread()->associate($this->thread);
        $reply->user()->associate($this->user);
        $reply->save();

        $this->assertEquals($this->user->username, $reply->author());
    }

    /** @test */
    public function a_reply_has_a_body()
    {
        $reply = factory(Reply::class)->make([
            'body' => 'foobar'
        ]);

        $reply->thread()->associate($this->thread);
        $reply->user()->associate($this->user);
        $reply->save();

        $this->assertSame('foobar', $reply->body);
    }

    /** @test */
    public function a_reply_can_be_liked()
    {
        $reply = factory(Reply::class)->make([
            'body' => 'foobar'
        ]);

        $reply->thread()->associate($this->thread);
        $reply->user()->associate($this->user);
        $reply->save();

        $like = factory(Like::class)->make();
        $reply->likes()->save($like);

        $this->assertCount(1, $reply->likes);
    }
}
