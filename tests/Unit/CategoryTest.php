<?php

namespace Tests\Unit;

use App\Category;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function a_category_has_many_threads()
    {
        $category = factory(Category::class)->create();

        $threads = factory(Thread::class, 10)->make();

        $category->threads()->saveMany($threads);

        $this->assertCount(10, $category->threads);
    }

    /** @test */
    public function a_category_has_a_path()
    {
        $category = factory(Category::class)->create([
            'name' => 'foo'
        ]);

        $this->assertEquals('/categories/foo', $category->path());
    }

    /** @test */
    public function a_category_has_a_name()
    {
        $category = factory(Category::class)->create([
            'name' => 'foo'
        ]);

        $this->assertSame('foo', $category->name);
    }

    /** @test */
    public function a_category_can_have_a_description()
    {
        $category = factory(Category::class)->create([
            'description' => 'foo'
        ]);

        $this->assertDatabaseHas('categories', [
            'description' => 'foo'
        ]);
    }
}
